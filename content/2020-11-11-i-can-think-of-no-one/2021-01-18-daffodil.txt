E major

I   - E
ii  - F#m
iii - G#m
IV  - A
V   - B
vi  - C#m
vii - D#dim

Try:
Chorus: A      - E   - B   - E
Verse:  A(F#m) - C#m - G#m - B(F#m) 

I was thinking we would
get together you could
meet me at the
border line

you could be my sweetheart 
we would come to be hard
pressed to make it
home on time 

and I want to save just one daffodil from the springtime
and you're gonna say we don't belong to the same side
and you're gonna say we won't want to put on a disguise
I don't, I won't, would you hear me out...

I can think of no one
can you think of someone
who would put it
on the line?

maybe it's a foregone 
conclusion it's all wrong
certainly it
crossed my mind

and I want to say I do believe it would be right
to come back together even if it's just for one night
and you're gonna see just how we could run off and be fine
we'll go, I know, it'll be alright...

we could start a new life
you and me we just might
find a place a-
way from strife

you could be my sweetheart
a loving home would be ours
we could build it
simple like

and we wouldn't look back, there's nothing here for us today
and we would start over we would see it was ok
and what do you say I don't think we ought to delay
we should, we could, be on our way...

I'll pick you a daisy
you may think I'm crazy,
we'll see if it's
really so

yes, no, maybe
skies are getting hazy
something we al-
ready know

but we could go taste just one little bit of the good life
and I'm pretty sure that there's no one else who could be mine
cause when we're together you make me feel like the springtime
we'll recover each other, me and you
