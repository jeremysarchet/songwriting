I remember this tune coming to me years ago, perhaps back in college, and it remained kicking around since. These words came with it:

"Fight, fight, satellite fight
burning brightly in the night"