#!/usr/bin/env bash

rm -rf public
cp -r content public
cp -r source/* public

cd public/

# Home page
echo "<html>
<head>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' type='text/css' href='style.css'/>
</head>
<body>
<h1>Song ideas</h1>
<ul>
" > ./index.html

for dir_path in */; do
  dir=$(basename $dir_path);
  date="${dir:0:10}"
  title=$(echo "${dir:11}" | sed 's/-/ /g')

  echo "<li><a href='./${dir}/index.html'>${date} ${title}</a></li>" >> ./index.html

  # Song page
  echo "<html>
<head>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' type='text/css' href='../style.css' />
  <script src='../index.js'></script>
</head>
<body>
<h1 class="song">${title}</h1>
<h2>${date}</h2>
" > "./${dir}/index.html"

  echo "<nav><a href='../index.html'>back</a></nav>" >> "./${dir}/index.html"

  for file_path in "${dir}/"*; do
    file=$(basename $file_path);

    # Check if the current item is a file
    if [[ -f $file_path && "$file_path" != */index.html ]]; then
      echo "<section>" >> "./${dir}/index.html"
      if [[ $file == *.txt ]]; then
        echo "<h3 class="attachment-title">${file}</h3>" >> "./${dir}/index.html"

        if [[ $file == *.abc.txt ]]; then
          echo "<pre class="abc">" >> "./${dir}/index.html"
        else
          echo "<pre>" >> "./${dir}/index.html"
        fi

        contents=$(cat $file_path);
        echo "${contents}</pre>" >> "./${dir}/index.html"
      fi
      if [[ $file == *.mp3 ]]; then
        echo "
<h3 class="attachment-title">${file}</h3>
<audio controls src="./${file}">
  <a href='./${file}'>${file}</a>
</audio>
        " >> "./${dir}/index.html"
      fi

      echo "</section>" >> "./${dir}/index.html"
    fi
  done

  echo "</li></body></html>" >> "./${dir}/index.html"
done

echo "</body></html>" >> ./index.html
