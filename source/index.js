/* Constucts a link to the abcjs "Quick Editor" for each ABC element
 *
 * Link is like: "https://editor.drawthedots.com/?t=<URL encoded content>";
 *
 */
function createABCLinks() {
  const elements = document.getElementsByClassName('abc');

  Array.from(elements).forEach((element) => {
    const encodedABC = encodeURIComponent(element.innerText);

    const link = document.createElement('a');
    link.href = `https://editor.drawthedots.com/?t=${encodedABC}`;
    link.target = '_blank';
    link.textContent = 'Open in editor';
    element.insertAdjacentElement('afterend', link);
  });
}

document.addEventListener('DOMContentLoaded', createABCLinks);
