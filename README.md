# Songwriting

View it live here: [https://jeremysarchet.gitlab.io/songwriting/](https://jeremysarchet.gitlab.io/songwriting/)

Usage: `./build.sh && open public/index.html`

## ABC Notation
- [Steve Mansfield's Abc music tutorial](http://www.lesession.co.uk/abc/abc_notation.htm)
- [abcjs: Quick Editor](https://editor.drawthedots.com/)

## Music tinkering

- [OneMotion Chord Player](https://www.onemotion.com/chord-player/)
- [Name That Key - Find a Song's Key by It's Chords](https://musictheorysite.com/namethatkey/)
- [Musical Scale Finder and Key Finder](https://www.scales-chords.com/scalefinder.php)
- [Online Sequencer](https://onlinesequencer.net/)

## Music theory

- [Time Signature Numbers: What do they mean?](https://www.mymusictheory.com/for-students/grade-5/48-3-time-signatures)
- [Why is the BPM of 12/8 songs expressed in dotted quarter notes?](https://www.reddit.com/r/musictheory/comments/jis97d/why_is_the_bpm_of_128_songs_expressed_in_dotted/)

## Other

- [How to allow directory listing on gitlab.com pages?](https://forum.gitlab.com/t/how-to-allow-directory-listing-on-gitlab-com-pages/20625/6)
